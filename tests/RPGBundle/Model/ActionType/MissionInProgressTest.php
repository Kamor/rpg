<?php

namespace Tests\RPGBundle\Model\ActionType;

use PHPUnit\Framework\TestCase;
use RPGBundle\Model\ActionType\MissionInProgress;

/**
 * Class SumServiceTest
 */
class MissionInProgressTest extends TestCase
{
    public $attributesArray = ['missionId' => 11];

    public $attributesJSON = '{"missionId":11}';

    public $service;

    public function setUp()
    {
        $this->service = new MissionInProgress();
    }

    public function testAttributesToJson()
    {
        self::assertEquals($this->attributesJSON, $this->service->attributesToJson($this->attributesArray));
    }

    public function testAttributesFromJson()
    {
        self::assertEquals($this->attributesArray, $this->service->attributesFromJson($this->attributesJSON));
    }
}
