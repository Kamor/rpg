### OBJECT 103

**It's year 2020.**

Biologic war caused by North Korea and it's allies aggression on South Korea bring a chaos to the world that hasn't been
predicted even by the greatest minds in the history of mankind. Hard situation on polish - chinese border and a lack of
response from the west world leave polish and german soldiers on front line without enough supplies and ammo. Unknown
mutations that are the effect of nanobots which were used on the battlefield for the first time in history leaves people
hopeless.

Your new name is OBJECT 103. You're one of the very last human being naturally resistant for the nanobots. North Korean
intelligence somehow learned about you. You are safe until sadistic scientists don't know the real source of your health
condition and it'd be good if it stay like this as long as possible.

You are waking up in a room that looks like hospital, in an incubator that was made especially for you.

### Unfinished for the first stage:
1. User should choose species after first mission - he can escape with the cpt. or kill him and escape alone. Then he could become an solider or a renegade.
2. Each species has his own perks and attribute modifiers - based on his gender and species a character can have more luck or bigger capacity for inventory.