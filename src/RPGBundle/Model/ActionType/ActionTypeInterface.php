<?php

namespace RPGBundle\Model\ActionType;

interface ActionTypeInterface
{
    /**
     * @param array $attributes
     *
     * @return string
     */
    public function attributesToJson(array $attributes): string;

    /**
     * @param string $data
     *
     * @return array
     */
    public function attributesFromJson(string $data): array;
}
