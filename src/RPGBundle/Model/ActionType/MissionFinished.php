<?php


namespace RPGBundle\Model\ActionType;

class MissionFinished implements ActionTypeInterface
{
    public static $identifier = 'mission_finished';

    protected $dataKeys = [
        'missionId',
    ];

    public function attributesToJson(array $attributes): string
    {
        return json_encode($attributes);
    }

    public function attributesFromJson(string $data): array
    {
        $data = json_decode($data, true);

        $attributes = [];
        foreach ($this->dataKeys as $dataKey) {
            $attributes[$dataKey] = $data[$dataKey];
        }

        return $attributes;
    }
}
