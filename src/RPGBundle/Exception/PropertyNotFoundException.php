<?php

namespace RPGBundle\Exception;

use Exception;

class PropertyNotFoundException extends Exception
{
    /**
     * @param string $property
     * @param string $class
     */
    public function __construct(string $property, string $class)
    {
        parent::__construct(sprintf('Property %s not found in class %s', $property, $class));
    }
}
