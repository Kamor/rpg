<?php

namespace RPGBundle\Exception;

use Exception;

class PropertyReadOnlyException extends Exception
{
}
