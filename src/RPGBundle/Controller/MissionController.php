<?php

namespace RPGBundle\Controller;

use RPGBundle\Service\DialogService;
use RPGBundle\Service\FigureService;
use RPGBundle\Service\MissionService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class MissionController extends Controller
{
    /** @var EngineInterface */
    protected $templating;

    /** @var AuthorizationCheckerInterface */
    protected $authorizationChecker;

    /** @var TokenStorageInterface */
    protected $tokenStorage;

    /** @var FigureService */
    protected $figureService;

    /** @var DialogService */
    protected $dialogService;

    /** @var MissionService */
    protected $missionService;

    /**
     * @param EngineInterface $templating
     * @param AuthorizationCheckerInterface $authorizationChecker
     * @param TokenStorageInterface $tokenStorage
     * @param FigureService $figureService
     * @param DialogService $dialogService
     * @param MissionService $missionService
     */
    public function __construct(
        EngineInterface $templating,
        AuthorizationCheckerInterface $authorizationChecker,
        TokenStorageInterface $tokenStorage,
        FigureService $figureService,
        DialogService $dialogService,
        MissionService $missionService
    ) {
        $this->templating = $templating;
        $this->authorizationChecker = $authorizationChecker;
        $this->tokenStorage = $tokenStorage;
        $this->figureService = $figureService;
        $this->dialogService = $dialogService;
        $this->missionService = $missionService;
    }

    /**
     * @return Response
     */
    public function playAction(): Response
    {
        $figure = $this->figureService->getUserFigure($this->tokenStorage->getToken()->getUser()->getId());

        $currentMission = $this->missionService->continue($figure);
        $sentence = $this->dialogService->getByMissionId($currentMission->getId());
        $answers = $this->dialogService->getAnswers($sentence);

        return $this->templating->renderResponse('RPGBundle:mission:play.html.twig', [
            'mission' => $currentMission,
            'sentence' => $sentence,
            'answers' => $answers,
        ]);
    }
}
