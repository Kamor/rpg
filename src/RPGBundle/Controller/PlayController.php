<?php

namespace RPGBundle\Controller;

use RPGBundle\Service\ActionService;
use RPGBundle\Service\FigureService;
use RPGBundle\Service\MissionService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Router;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class PlayController extends Controller
{
    /** @var EngineInterface */
    protected $templating;

    /** @var TokenStorageInterface */
    protected $tokenStorage;

    /** @var FigureService */
    protected $figureService;

    /** @var ActionService */
    protected $actionService;

    /** @var MissionService */
    protected $missionService;

    /** @var Router */
    protected $router;

    /**
     * @param EngineInterface $templating
     * @param TokenStorageInterface $tokenStorage
     * @param FigureService $figureService
     * @param ActionService $actionService
     * @param MissionService $missionService
     * @param Router $router
     */
    public function __construct(
        EngineInterface $templating,
        TokenStorageInterface $tokenStorage,
        FigureService $figureService,
        ActionService $actionService,
        MissionService $missionService,
        Router $router
    ) {
        $this->templating = $templating;
        $this->tokenStorage = $tokenStorage;
        $this->figureService = $figureService;
        $this->actionService = $actionService;
        $this->missionService = $missionService;
        $this->router = $router;
    }

    /**
     * @return Response
     */
    public function playAction(): Response
    {
        $figure = $this->figureService->getUserFigure($this->tokenStorage->getToken()->getUser()->getId());

        if (null === $figure) {
            return new RedirectResponse(
                $this->router->generate('rpg_figure_add_basic')
            );
        }

        $currentMission = $this->missionService->continue($figure);

        return $this->templating->renderResponse('RPGBundle:play:mission.html.twig', [
            'mission' => $currentMission,
        ]);
    }
}
