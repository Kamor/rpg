<?php

namespace RPGBundle\Controller;

use RPGBundle\Form\FigureType;
use RPGBundle\Persistence\ValueObject\Figure;
use RPGBundle\Service\FigureService;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Router;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class FigureController
{
    /** @var EngineInterface */
    protected $templating;

    /** @var AuthorizationCheckerInterface */
    protected $authorizationChecker;

    /** @var TokenStorageInterface */
    protected $tokenStorage;

    /** @var FormFactoryInterface */
    protected $formFactory;

    /** @var FigureService */
    protected $figureService;

    /** @var Router */
    protected $router;

    /**
     * @param EngineInterface $templating
     * @param AuthorizationCheckerInterface $authorizationChecker
     * @param TokenStorageInterface $tokenStorage
     * @param FormFactoryInterface $formFactory
     * @param FigureService $figureService
     * @param Router $router
     */
    public function __construct(
        EngineInterface $templating,
        AuthorizationCheckerInterface $authorizationChecker,
        TokenStorageInterface $tokenStorage,
        FormFactoryInterface $formFactory,
        FigureService $figureService,
        Router $router
    ) {
        $this->templating = $templating;
        $this->authorizationChecker = $authorizationChecker;
        $this->tokenStorage = $tokenStorage;
        $this->formFactory = $formFactory;
        $this->figureService = $figureService;
        $this->router = $router;
    }

    /**
     * @param Request $request
     *
     * @return Response
     */
    public function detailsAction(Request $request): Response
    {
        $form = $this->formFactory
            ->createBuilder(FigureType::class)
            ->add('save', SubmitType::class, array('label' => 'Start game'))
            ->getForm()
            ->handleRequest($request)
        ;

        if ($form->isSubmitted() && $form->isValid()) {
            $figure = new Figure([
                'name' => $form->getData()->getName(),
                'gender' => $form->getData()->getGender(),
                'user' => $this->tokenStorage->getToken()->getUser()->getId(),
                'health'=> Figure::HEALTH[$form->getData()->getGender()],
                'exp' => 0,
                'level' => 0,
                'actionPoints' => Figure::ACTION_POINTS,
                'capacity' => Figure::CAPACITY[$form->getData()->getGender()],
            ]);

            $figureId = $this->figureService->add($figure);
            if ($figureId) {
                return new RedirectResponse(
                    $this->router->generate('rpg_homepage')
                );
            }
        }

        return $this->templating->renderResponse('RPGBundle:figure:add.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
