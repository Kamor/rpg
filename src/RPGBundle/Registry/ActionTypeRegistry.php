<?php

namespace RPGBundle\Registry;

use RPGBundle\Model\ActionType\ActionTypeInterface;

class ActionTypeRegistry
{
    /** @var array */
    private $types;

    /**
     * @param string $identifier
     * @param ActionTypeInterface $type
     */
    public function addActionType($identifier, $type)
    {
        $this->types[$identifier] = $type;
    }

    /**
     * @param string $identifier
     *
     * @return ActionTypeInterface
     */
    public function getActionType(string $identifier): ActionTypeInterface
    {
        return $this->types[$identifier];
    }

    /**
     * @return ActionTypeInterface[]
     */
    public function getActionTypes(): array
    {
        return $this->types;
    }
}
