<?php

namespace RPGBundle;

use RPGBundle\DependencyInjection\Compiler\ActionTypePass;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class RPGBundle
 */
class RPGBundle extends Bundle
{
    public function build(ContainerBuilder $container)
    {
        parent::build($container);

        $container->addCompilerPass(new ActionTypePass());
    }
}
