<?php

namespace RPGBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

class ActionTypePass implements CompilerPassInterface
{
    /**
     * @param ContainerBuilder $container
     */
    public function process(ContainerBuilder $container)
    {
        if (!$container->has('rpg.registry.action_type')) {
            return;
        }

        $registry = $container->findDefinition('rpg.registry.action_type');
        foreach ($container->findTaggedServiceIds('rpg.action_type') as $id => $tags) {
            foreach ($tags as $tag) {
                $registry->addMethodCall('addActionType', [$tag['alias'], new Reference($id)]);
            }
        }
    }
}
