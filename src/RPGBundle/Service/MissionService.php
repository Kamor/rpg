<?php

namespace RPGBundle\Service;

use DateTime;
use RPGBundle\Entity\Mission;
use RPGBundle\Model\ActionType\MissionFinished;
use RPGBundle\Model\ActionType\MissionInProgress;
use RPGBundle\Persistence\ValueObject\Action;
use RPGBundle\Persistence\ValueObject\Figure;

class MissionService extends AbstractService
{
    public function setEntity()
    {
        $this->repository = $this->doctrine->getRepository('RPGBundle:Mission');
    }

    public function continue(Figure $figure)
    {
        return $this->getCurrentMission($figure->id);
    }

    public function getCurrentMission($figureId)
    {
        $actionService = $this->container->get('rpg.service.action');
        $actionTypeRegistry = $this->container->get('rpg.registry.action_type');
        $lastAction = $actionService->getLastAction($figureId, MissionInProgress::$identifier);

        if (!$lastAction) {
            $lastAction = $actionService->getLastAction($figureId, MissionFinished::$identifier, 1);
            $data = $actionTypeRegistry->getActionType($lastAction->getType())->attributesFromJson($lastAction->getData());

            $nextMissionId = $lastAction
                ? $this->repository->getNextMissionId($data['missionId'])
                : 0;

            $this->startNewMission($figureId, $actionTypeRegistry->getActionType(MissionInProgress::$identifier)->attributesToJson([
                'missionId' => $nextMissionId,
            ]));
        }

        $data = $actionTypeRegistry->getActionType($lastAction->getType())->attributesFromJson($lastAction->getData());
        $mission = $this->repository->find($data['missionId']);

        return $mission;
    }

    public function startNewMission($figureId, $data)
    {
        $actionService = $this->container->get('rpg.service.action');

        return $actionService->addAction(new Action([
            'figure' => $figureId,
            'date' => new DateTime(),
            'data' => $data,
            'type' => MissionInProgress::$identifier,
            'status' => Action::STATUS_PROGRESS,
        ]));
    }
}
