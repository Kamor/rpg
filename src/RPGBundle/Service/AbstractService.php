<?php

namespace RPGBundle\Service;

use Doctrine\Bundle\DoctrineBundle\Registry;
use Doctrine\Common\Persistence\ObjectRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

abstract class AbstractService
{
    /** @var Registry  */
    protected $doctrine;

    /** @var ContainerInterface */
    protected $container;

    /** @var EntityManagerInterface  */
    protected $entityManager;

    /** @var ObjectRepository  */
    protected $repository;

    /**
     * @param Registry $doctrine
     */
    public function __construct(Registry $doctrine, ContainerInterface $container)
    {
        $this->doctrine = $doctrine;
        $this->container = $container;
        $this->entityManager = $doctrine->getManager();
    }

    /**
     * @param int|null $id
     *
     * @return mixed
     */
    public function get($id = null)
    {
        return (null === $id) ? $this->getAll() : $this->get($id);
    }

    /**
     * @param int $id
     *
     * @return object
     */
    public function find(int $id): object
    {
        return $this->repository->find($id);
    }

    /**
     * @return array
     */
    public function findAll(): array
    {
        return $this->repository->findAll();
    }

    abstract public function setEntity();
}
