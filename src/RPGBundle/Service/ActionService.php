<?php

namespace RPGBundle\Service;

use RPGBundle\Persistence\ValueObject\Action;
use RPGBundle\Entity\Action as ActionEntity;

class ActionService extends AbstractService
{
    public function setEntity()
    {
        $this->repository = $this->doctrine->getRepository('RPGBundle:Action');
    }

    public function getLastAction($figureId, $typeIdentifier, $status = 0)
    {
        $action = $this->repository->findBy([
                'figure' => $figureId,
                'type' => $typeIdentifier,
                'status' => $status,
            ],
            ['id' => 'desc'],
            1
        );

        return reset($action);
    }

    public function addAction(Action $action)
    {
        $entity = new ActionEntity();

        $entity
            ->setFigure($action->figure)
            ->setDate($action->date)
            ->setData($action->data)
            ->setType($action->type)
            ->setStatus($action->status)
        ;

        $this->entityManager->persist($entity);
        $this->entityManager->flush();

        return $entity->getId();
    }
}
