<?php

namespace RPGBundle\Service;

use RPGBundle\Persistence\ValueObject\Figure;
use RPGBundle\Entity\Figure as FigureEntity;
use RPGBundle\Repository\FigureRepository;

/**
 * Class TestService
 */
class FigureService extends AbstractService
{
    /** @var FigureRepository entity */
    protected $repository;


    public function setEntity()
    {
        $this->repository = $this->doctrine->getRepository('RPGBundle:Figure');
    }

    public function add(Figure $figure): int
    {
        if ($this->exists($figure->user)) {
            return 0;
        }

        $entity = new FigureEntity();

        $entity
            ->setUser($figure->user)
            ->setName($figure->name)
            ->setGender($figure->gender)
            ->setHealth($figure->health)
            ->setExp($figure->exp)
            ->setLevel($figure->level)
            ->setActionPoints($figure->actionPoints)
            ->setCapacity($figure->capacity)
        ;

        $this->entityManager->persist($entity);
        $this->entityManager->flush();

        return $entity->getId();
    }

    public function get($id = null): Figure
    {
        if (null === $figure = $this->repository->find($id)) {
            return new Figure();
        }

        return (new Figure)->makeFromEntity($figure);
    }

    public function getUserFigure($userId)
    {
        if (null === $figure = $this->repository->findByUser($userId)) {
            return null;
        }

        return (new Figure)->makeFromEntity($figure);
    }

    /**
     * @param int $userId
     *
     * @return int
     */
    public function exists($userId): int
    {
        $entities = $this->repository->findBy([
            'user' => $userId
        ]);

        return count($entities);
    }
}
