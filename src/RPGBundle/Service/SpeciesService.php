<?php

namespace RPGBundle\Service;

/**
 * Class TestService
 */
class SpeciesService extends AbstractService
{
    public function setEntity()
    {
        $this->repository = $this->doctrine->getRepository('RPGBundle:Species');
    }
}
