<?php

namespace RPGBundle\Service;

use Doctrine\Common\Persistence\ObjectRepository;
use RPGBundle\Persistence\ValueObject\Figure;

class DialogService extends AbstractService
{
    /** @var ObjectRepository  */
    protected $sentenceRepository;

    /** @var ObjectRepository  */
    protected $sentenceOptionRepository;

    public function setEntity()
    {
        $this->repository = $this->doctrine->getRepository('RPGBundle:Dialog');
        $this->sentenceRepository = $this->doctrine->getRepository('RPGBundle:Sentence');
        $this->sentenceOptionRepository = $this->doctrine->getRepository('RPGBundle:SentenceOption');
    }

    public function getByMissionId($missionId)
    {
        $dialog = $this->repository->findOneBy(['mission' => $missionId]);

        return $this->getSentence($dialog);
    }

    public function getSentence($dialog)
    {
        $sentence = $this->sentenceRepository->findBy(
            ['dialog' => $dialog],
            ['id' => 'ASC'],
            1
        );

        return reset($sentence);
    }

    public function getAnswers($sentence)
    {
        $next = $this->sentenceOptionRepository->findBy(['sentence' => $sentence]);

        return $this->sentenceRepository->findAnswers(array_map(function ($item) {
            return $item->getAnswer();
        }, $next));
    }
}
