<?php

namespace RPGBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Dialog
 *
 * @ORM\Table(name="dialog")
 * @ORM\Entity(repositoryClass="RPGBundle\Repository\DialogRepository")
 */
class Dialog
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="npc", type="integer")
     */
    private $npc;

    /**
     * @var int
     *
     * @ORM\Column(name="mission", type="integer")
     */
    private $mission;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set npc
     *
     * @param integer $npc
     *
     * @return Dialog
     */
    public function setNpc($npc)
    {
        $this->npc = $npc;

        return $this;
    }

    /**
     * Get npc
     *
     * @return int
     */
    public function getNpc()
    {
        return $this->npc;
    }
    /**
     * Set mission
     *
     * @param integer $mission
     *
     * @return Dialog
     */
    public function setMission($mission)
    {
        $this->mission = $mission;

        return $this;
    }

    /**
     * Get mission
     *
     * @return int
     */
    public function getMission()
    {
        return $this->mission;
    }
}
