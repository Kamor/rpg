<?php

namespace RPGBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Sentence
 *
 * @ORM\Table(name="sentence")
 * @ORM\Entity(repositoryClass="RPGBundle\Repository\SentenceRepository")
 */
class Sentence
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="dialog", type="integer")
     */
    private $dialog;

    /**
     * @var int
     *
     * @ORM\Column(name="speaker", type="integer")
     */
    private $speaker;

    /**
     * @var string
     *
     * @ORM\Column(name="text", type="text")
     */
    private $text;

    /**
     * @var int
     *
     * @ORM\Column(name="action", type="integer")
     */
    private $action;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dialog
     *
     * @param integer $dialog
     *
     * @return Sentence
     */
    public function setDialog($dialog)
    {
        $this->dialog = $dialog;

        return $this;
    }

    /**
     * Get dialog
     *
     * @return int
     */
    public function getDialog()
    {
        return $this->dialog;
    }

    /**
     * Set speaker
     *
     * @param integer $speaker
     *
     * @return Sentence
     */
    public function setSpeaker($speaker)
    {
        $this->speaker = $speaker;

        return $this;
    }

    /**
     * Get speaker
     *
     * @return int
     */
    public function getSpeaker()
    {
        return $this->speaker;
    }

    /**
     * Set text
     *
     * @param string $text
     *
     * @return Sentence
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get text
     *
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set action
     *
     * @param int $action
     *
     * @return Sentence
     */
    public function setAction($action)
    {
        $this->action = $action;

        return $this;
    }

    /**
     * Get action
     *
     * @return int
     */
    public function getAction()
    {
        return $this->action;
    }
}
