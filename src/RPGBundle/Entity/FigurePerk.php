<?php

namespace RPGBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * FigurePerk
 *
 * @ORM\Table(name="figure_perk")
 * @ORM\Entity(repositoryClass="RPGBundle\Repository\FigurePerkRepository")
 */
class FigurePerk
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="figure", type="integer")
     * @ORM\ManyToOne(targetEntity="Figure", inversedBy="figure")
     */
    private $figure;

    /**
     * @var int
     *
     * @ORM\Column(name="perk", type="integer")
     * @ORM\ManyToOne(targetEntity="Perk")
     * @ORM\JoinColumn(name="perk", referencedColumnName="id")
     */
    private $perk;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set figure
     *
     * @param integer $figure
     *
     * @return FigurePerk
     */
    public function setFigure($figure)
    {
        $this->figure = $figure;

        return $this;
    }

    /**
     * Get figure
     *
     * @return int
     */
    public function getFigure()
    {
        return $this->figure;
    }

    /**
     * Set perk
     *
     * @param integer $perk
     *
     * @return FigurePerk
     */
    public function setPerk($perk)
    {
        $this->perk = $perk;

        return $this;
    }

    /**
     * Get perk
     *
     * @return int
     */
    public function getPerk()
    {
        return $this->perk;
    }
}
