<?php

namespace RPGBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * FigureAttribute
 *
 * @ORM\Table(name="figure_attribute")
 * @ORM\Entity(repositoryClass="RPGBundle\Repository\FigureAttributeRepository")
 */
class FigureAttribute
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="figure", type="integer")
     * @ORM\ManyToOne(targetEntity="Figure", inversedBy="figure")
     */
    private $figure;

    /**
     * @var int
     *
     * @ORM\Column(name="attribute", type="integer")
     * @ORM\ManyToOne(targetEntity="Attribute")
     * @ORM\JoinColumn(name="attribute", referencedColumnName="id")
     */
    private $attribute;

    /**
     * @var int
     *
     * @ORM\Column(name="value", type="integer")
     */
    private $value;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set figure
     *
     * @param integer $figure
     *
     * @return FigureAttribute
     */
    public function setFigure($figure)
    {
        $this->figure = $figure;

        return $this;
    }

    /**
     * Get figure
     *
     * @return int
     */
    public function getFigure()
    {
        return $this->figure;
    }

    /**
     * Set attribute
     *
     * @param integer $attribute
     *
     * @return FigureAttribute
     */
    public function setAttribute($attribute)
    {
        $this->attribute = $attribute;

        return $this;
    }

    /**
     * Get attribute
     *
     * @return int
     */
    public function getAttribute()
    {
        return $this->attribute;
    }

    /**
     * Set value
     *
     * @param integer $value
     *
     * @return FigureAttribute
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return int
     */
    public function getValue()
    {
        return $this->value;
    }
}
