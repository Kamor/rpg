<?php

namespace RPGBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * NPC
 *
 * @ORM\Table(name="n_p_c")
 * @ORM\Entity(repositoryClass="RPGBundle\Repository\NPCRepository")
 */
class NPC
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, unique=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @var float
     *
     * @ORM\Column(name="health", type="float")
     */
    private $health;

    /**
     * @var int
     *
     * @ORM\Column(name="level", type="integer")
     */
    private $level;

    /**
     * @var int
     *
     * @ORM\Column(name="actionPoints", type="integer")
     */
    private $actionPoints;

    /**
     * @var int
     *
     * @ORM\Column(name="nature", type="integer")
     */
    private $nature;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return NPC
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return NPC
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set health
     *
     * @param float $health
     *
     * @return NPC
     */
    public function setHealth($health)
    {
        $this->health = $health;

        return $this;
    }

    /**
     * Get health
     *
     * @return float
     */
    public function getHealth()
    {
        return $this->health;
    }

    /**
     * Set level
     *
     * @param integer $level
     *
     * @return NPC
     */
    public function setLevel($level)
    {
        $this->level = $level;

        return $this;
    }

    /**
     * Get level
     *
     * @return int
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * Set actionPoints
     *
     * @param integer $actionPoints
     *
     * @return NPC
     */
    public function setActionPoints($actionPoints)
    {
        $this->actionPoints = $actionPoints;

        return $this;
    }

    /**
     * Get actionPoints
     *
     * @return int
     */
    public function getActionPoints()
    {
        return $this->actionPoints;
    }

    /**
     * Set nature
     *
     * @param integer $nature
     *
     * @return NPC
     */
    public function setNature($nature)
    {
        $this->nature = $nature;

        return $this;
    }

    /**
     * Get nature
     *
     * @return int
     */
    public function getNature()
    {
        return $this->nature;
    }
}
