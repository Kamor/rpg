<?php

namespace RPGBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SentenceOption
 *
 * @ORM\Table(name="sentence_option")
 * @ORM\Entity(repositoryClass="RPGBundle\Repository\SentenceOptionRepository")
 */
class SentenceOption
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="sentence", type="integer")
     * @ORM\ManyToOne(targetEntity="Sentence")
     * @ORM\JoinColumn(name="sentence", referencedColumnName="id")
     */
    private $sentence;

    /**
     * @var int
     *
     * @ORM\Column(name="answer", type="integer")
     * @ORM\ManyToOne(targetEntity="Sentence")
     * @ORM\JoinColumn(name="answer", referencedColumnName="id")
     */
    private $answer;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set sentence
     *
     * @param integer $sentence
     *
     * @return SentenceOption
     */
    public function setSentence($sentence)
    {
        $this->sentence = $sentence;

        return $this;
    }

    /**
     * Get sentence
     *
     * @return int
     */
    public function getSentence()
    {
        return $this->sentence;
    }

    /**
     * Set answer
     *
     * @param integer $answer
     *
     * @return SentenceOption
     */
    public function setAnswer($answer)
    {
        $this->answer = $answer;

        return $this;
    }

    /**
     * Get answer
     *
     * @return int
     */
    public function getAnswer()
    {
        return $this->answer;
    }
}
