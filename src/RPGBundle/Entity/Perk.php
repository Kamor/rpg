<?php

namespace RPGBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Perk
 *
 * @ORM\Table(name="perk")
 * @ORM\Entity(repositoryClass="RPGBundle\Repository\PerkRepository")
 */
class Perk
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, unique=true)
     */
    private $name;

    /**
     * @var int
     *
     * @ORM\Column(name="species", type="integer")
     * @ORM\ManyToOne(targetEntity="Species")
     * @ORM\JoinColumn(name="species", referencedColumnName="id")
     */
    private $species;

    /**
     * @var int
     *
     * @ORM\Column(name="modifier", type="integer")
     */
    private $modifier;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Perk
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set species
     *
     * @param integer $species
     *
     * @return Perk
     */
    public function setSpecies($species)
    {
        $this->species = $species;

        return $this;
    }

    /**
     * Get species
     *
     * @return int
     */
    public function getSpecies()
    {
        return $this->species;
    }

    /**
     * Set modifier
     *
     * @param integer $modifier
     *
     * @return Perk
     */
    public function setModifier($modifier)
    {
        $this->modifier = $modifier;

        return $this;
    }

    /**
     * Get modifier
     *
     * @return int
     */
    public function getModifier()
    {
        return $this->modifier;
    }
}
