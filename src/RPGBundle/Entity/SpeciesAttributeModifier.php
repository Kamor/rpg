<?php

namespace RPGBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SpeciesAttributeModifier
 *
 * @ORM\Table(name="species_attribute_modifier")
 * @ORM\Entity(repositoryClass="RPGBundle\Repository\SpeciesAttributeModifierRepository")
 */
class SpeciesAttributeModifier
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="species", type="integer")
     * @ORM\ManyToOne(targetEntity="Species")
     * @ORM\JoinColumn(name="species", referencedColumnName="id")
     */
    private $species;

    /**
     * @var int
     *
     * @ORM\Column(name="attribute", type="integer")
     * @ORM\ManyToOne(targetEntity="Attribute")
     * @ORM\JoinColumn(name="attribute", referencedColumnName="id")
     */
    private $attribute;

    /**
     * @var int
     *
     * @ORM\Column(name="modifier", type="integer")
     */
    private $modifier;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set species
     *
     * @param integer $species
     *
     * @return SpeciesAttributeModifier
     */
    public function setSpecies($species)
    {
        $this->species = $species;

        return $this;
    }

    /**
     * Get species
     *
     * @return int
     */
    public function getSpecies()
    {
        return $this->species;
    }

    /**
     * Set attribute
     *
     * @param integer $attribute
     *
     * @return SpeciesAttributeModifier
     */
    public function setAttribute($attribute)
    {
        $this->attribute = $attribute;

        return $this;
    }

    /**
     * Get attribute
     *
     * @return int
     */
    public function getAttribute()
    {
        return $this->attribute;
    }

    /**
     * Set modifier
     *
     * @param integer $modifier
     *
     * @return SpeciesAttributeModifier
     */
    public function setModifier($modifier)
    {
        $this->modifier = $modifier;

        return $this;
    }

    /**
     * Get modifier
     *
     * @return int
     */
    public function getModifier()
    {
        return $this->modifier;
    }
}
