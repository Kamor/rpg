<?php

namespace RPGBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * NPCItem
 *
 * @ORM\Table(name="n_p_c_item")
 * @ORM\Entity(repositoryClass="RPGBundle\Repository\NPCItemRepository")
 */
class NPCItem
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="npc", type="integer")
     * @ORM\ManyToOne(targetEntity="NPC")
     * @ORM\JoinColumn(name="npc", referencedColumnName="id")
     */
    private $npc;

    /**
     * @var int
     *
     * @ORM\Column(name="item", type="integer")
     * @ORM\ManyToOne(targetEntity="Item")
     * @ORM\JoinColumn(name="item", referencedColumnName="id")
     */
    private $item;

    /**
     * @var int
     *
     * @ORM\Column(name="count", type="integer")
     */
    private $count;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set npc
     *
     * @param integer $npc
     *
     * @return NPCItem
     */
    public function setNpc($npc)
    {
        $this->npc = $npc;

        return $this;
    }

    /**
     * Get npc
     *
     * @return int
     */
    public function getNpc()
    {
        return $this->npc;
    }

    /**
     * Set item
     *
     * @param integer $item
     *
     * @return NPCItem
     */
    public function setItem($item)
    {
        $this->item = $item;

        return $this;
    }

    /**
     * Get item
     *
     * @return int
     */
    public function getItem()
    {
        return $this->item;
    }

    /**
     * Set count
     *
     * @param integer $count
     *
     * @return NPCItem
     */
    public function setCount($count)
    {
        $this->count = $count;

        return $this;
    }

    /**
     * Get count
     *
     * @return int
     */
    public function getCount()
    {
        return $this->count;
    }
}
