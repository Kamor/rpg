<?php

namespace RPGBundle\Persistence\ValueObject;

use RPGBundle\Persistence\AbstractValueObject;

class Dialog extends AbstractValueObject
{
    /** @var int */
    public $id;

    /** @var NPC */
    public $npc;
}
