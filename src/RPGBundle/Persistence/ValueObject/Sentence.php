<?php

namespace RPGBundle\Persistence\ValueObject;

use RPGBundle\Persistence\AbstractValueObject;

class Sentence extends AbstractValueObject
{
    /** @var int */
    public $id;

    /** @var int */
    public $dialog;

    /** @var int */
    public $speaker;

    /** @var Sentence */
    public $sentence;

    /** @var array */
    public $options;

    /** @var string */
    public $action;
}
