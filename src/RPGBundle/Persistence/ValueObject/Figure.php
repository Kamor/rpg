<?php

namespace RPGBundle\Persistence\ValueObject;

use RPGBundle\Entity\Figure as FigureEntity;
use RPGBundle\Persistence\AbstractValueObject;

class Figure extends AbstractValueObject
{
    const HEALTH = [
        'male' => 85,
        'female' => 95,
    ];

    const CAPACITY = [
        'male' => 200,
        'female' => 170,
    ];

    const ACTION_POINTS = 6;

    /** @var int */
    public $id;

    /** @var int */
    public $user;

    /** @var string */
    public $name;

    /** @var Species */
    public $species;

    /** @var string */
    public $gender;

    /** @var float */
    public $health;

    /** @var int */
    public $exp;

    /** @var int */
    public $level;

    /** @var int */
    public $actionPoints;

    /** @var float */
    public $capacity;

    /**
     * @param FigureEntity $figure
     *
     * @return Figure
     */
    public function makeFromEntity(FigureEntity $figure): Figure
    {
        return new self([
            'id' => $figure->getId(),
            'user' => $figure->getUser(),
            'name' => $figure->getName(),
            'species' => $figure->getSpecies(),
            'gender' => $figure->getGender(),
            'health' => $figure->getHealth(),
            'exp' => $figure->getExp(),
            'level' => $figure->getLevel(),
            'actionPoints' => $figure->getActionPoints(),
            'capacity' => $figure->getCapacity(),
        ]);
    }
}
