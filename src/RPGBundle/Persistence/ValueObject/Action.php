<?php

namespace RPGBundle\Persistence\ValueObject;

use RPGBundle\Persistence\AbstractValueObject;

class Action extends AbstractValueObject
{
    const STATUS_DONE = 1;
    const STATUS_PROGRESS = 0;

    /** @var int */
    public $id;

    /** @var int */
    public $figure;

    /** @var \DateTime */
    public $date;

    /** @var string */
    public $type;

    /** @var string */
    public $data;

    /** @var int */
    public $status;
}
