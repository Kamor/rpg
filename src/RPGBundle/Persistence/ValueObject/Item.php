<?php

namespace RPGBundle\Persistence\ValueObject;

use RPGBundle\Persistence\AbstractValueObject;

class Item extends AbstractValueObject
{
    /** @var int */
    public $id;

    /** @var ItemType */
    public $type;

    /** @var float */
    public $weight;

    /** @var float */
    public $value;
}
