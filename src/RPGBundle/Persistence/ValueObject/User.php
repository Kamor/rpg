<?php

namespace RPGBundle\Persistence\ValueObject;

use Doctrine\Common\Collections\Collection;
use RPGBundle\Persistence\AbstractValueObject;

class User extends AbstractValueObject
{
    /** @var int */
    public $id;

    /** @var string */
    public $username;

    /** @var string */
    public $usernameCanonical;

    /** @var string */
    public $email;

    /** @var string */
    public $emailCanonical;

    /** @var bool */
    public $enabled;

    /** @var int */
    public $lastLogin;

    /** @var Collection */
    public $groups;

    /** @var array */
    public $roles;
}
