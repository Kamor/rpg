<?php

namespace RPGBundle\Persistence\ValueObject;

use RPGBundle\Persistence\AbstractValueObject;

class NPC extends AbstractValueObject
{
    /** @var int */
    public $id;

    /** @var string */
    public $name;

    /** @var float */
    public $health;

    /** @var int */
    public $level;

    /** @var int */
    public $actionPoints;

    /** @var int */
    public $nature;
}
