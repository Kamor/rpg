<?php

namespace RPGBundle\Persistence\ValueObject;

use RPGBundle\Persistence\AbstractValueObject;

class FigurePerk extends AbstractValueObject
{
    /** @var int */
    public $id;

    /** @var Figure */
    public $figure;

    /** @var Perk */
    public $perk;
}
