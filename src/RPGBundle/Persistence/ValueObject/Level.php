<?php

namespace RPGBundle\Persistence\ValueObject;

use RPGBundle\Persistence\AbstractValueObject;

class Level extends AbstractValueObject
{
    /** @var int */
    public $id;

    /** @var float */
    public $exp;
}
