<?php

namespace RPGBundle\Persistence\ValueObject;

use RPGBundle\Persistence\AbstractValueObject;

class Mission extends AbstractValueObject
{
    /** @var int */
    public $id;

    /** @var string */
    public $name;

    /** @var string */
    public $description;

    /** @var int */
    public $exp;
}
