<?php

namespace RPGBundle\Persistence\ValueObject;

use RPGBundle\Persistence\AbstractValueObject;

class Species extends AbstractValueObject
{
    /** @var int */
    public $id;

    /** @var string */
    public $name;
}
