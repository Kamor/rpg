<?php

namespace RPGBundle\Persistence\ValueObject;

use RPGBundle\Persistence\AbstractValueObject;

class Perk extends AbstractValueObject
{
    /** @var int */
    public $id;

    /** @var string */
    public $name;

    /** @var Species */
    public $species;

    /** @var PerkModifier */
    public $modifier;
}
