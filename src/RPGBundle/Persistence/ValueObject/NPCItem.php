<?php

namespace RPGBundle\Persistence\ValueObject;

use RPGBundle\Persistence\AbstractValueObject;

class NPCItem extends AbstractValueObject
{
    /** @var int */
    public $id;

    /** @var NPC */
    public $npc;

    /** @var Item */
    public $item;

    /** @var int */
    public $count;
}
