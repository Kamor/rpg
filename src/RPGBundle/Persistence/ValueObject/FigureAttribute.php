<?php

namespace RPGBundle\Persistence\ValueObject;

use RPGBundle\Persistence\AbstractValueObject;

class FigureAttribute extends AbstractValueObject
{
    /** @var int */
    public $id;

    /** @var Figure */
    public $figure;

    /** @var Attribute */
    public $attribute;

    /** @var int */
    public $value;
}
