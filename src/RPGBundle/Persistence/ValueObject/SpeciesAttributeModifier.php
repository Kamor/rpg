<?php

namespace RPGBundle\Persistence\ValueObject;

use RPGBundle\Persistence\AbstractValueObject;

class SpeciesAttributeModifier extends AbstractValueObject
{
    /** @var int */
    public $id;

    /** @var Species */
    public $species;

    /** @var Attribute */
    public $attribute;

    /** @var int */
    public $modifier;
}
