<?php

namespace RPGBundle\Persistence\ValueObject;

use RPGBundle\Persistence\AbstractValueObject;

class Attribute extends AbstractValueObject
{
    /** @var int */
    public $id;

    /** @var string */
    public $name;
}
