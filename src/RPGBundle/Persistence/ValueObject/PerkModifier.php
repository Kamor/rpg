<?php

namespace RPGBundle\Persistence\ValueObject;

use RPGBundle\Persistence\AbstractValueObject;

class PerkModifier extends AbstractValueObject
{
    /** @var int */
    public $id;

    /** @var Attribute */
    public $attribute;

    /** @var int */
    public $value;
}
