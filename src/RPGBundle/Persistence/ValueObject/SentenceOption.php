<?php

namespace RPGBundle\Persistence\ValueObject;

use RPGBundle\Persistence\AbstractValueObject;

class SentenceOption extends AbstractValueObject
{
    /** @var int */
    public $id;

    /** @var int */
    public $sentence;

    /** @var int */
    public $answer;
}
